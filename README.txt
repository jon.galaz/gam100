These are the first games I did. PigiDen Life, the best of these 3 and the only one I made in a group,
doesn�t have an executable, but luckily I have the trailer and I can show more or less what I did:

	- I did the music of the trailer.
	- I did the movement and a bit of the animation system.
	- I did the chairs movement and the door openings.
	- I did the "IA" for the eye that follows the player and shoots at him.
	- I did the design of all the maps and levels.
	- I did the dash mechanic.
	- I did almost all the sprites.